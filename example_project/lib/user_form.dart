// ignore_for_file: prefer_const_constructors, avoid_print, no_logic_in_create_state, must_be_immutable

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class UserForm extends StatefulWidget {
  String userId;
  UserForm({Key? key, required this.userId}) : super(key: key);

  @override
  _UserFormState createState() => _UserFormState(this.userId);
}

class _UserFormState extends State<UserForm> {
  String userId;
  String fullname = "";
  String company = "";
  String age = "0";
  CollectionReference users = FirebaseFirestore.instance.collection('users');
  _UserFormState(this.userId);
  TextEditingController _fullNameController = new TextEditingController();
  TextEditingController _companyController = new TextEditingController();
  TextEditingController _ageController = new TextEditingController();
  @override
  void initState() {
    super.initState();
    if (userId.isNotEmpty) {
      users.doc(userId).get().then((snapshot) {
        if (snapshot.exists) {
          // print(snapshot.data());
          var data = snapshot.data() as Map<String, dynamic>;
          fullname = data['full_name'];
          company = data['company'];
          age = data['age'].toString();
          _fullNameController.text = fullname;
          _companyController.text = company;
          _ageController.text = age;
        }
      });
    }
  }

  final _formKey = GlobalKey<FormState>();
  Future<void> addUser() {
    return users
        .add({'full_name': fullname, 'company': company, 'age': int.parse(age)})
        .then((value) => print('User Added'))
        .catchError((error) => print('Faild to add user: $error '));
  }

  Future<void> updateUser() {
    return users
        .doc(userId)
        .update(
            {'company': company, 'full_name': fullname, 'age': int.parse(age)})
        .then((value) => print("User Updated"))
        .catchError((error) => print("Failed to update user: $error"));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('User'),
      ),
      body: Form(
          autovalidateMode: AutovalidateMode.onUserInteraction,
          key: _formKey,
          child: Column(
            children: [
              TextFormField(
                controller: _fullNameController,
                decoration: InputDecoration(labelText: 'Full Name'),
                onChanged: (value) {
                  setState(() {
                    fullname = value;
                  });
                },
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please input Name';
                  }
                  return null;
                },
              ),
              TextFormField(
                controller: _companyController,
                decoration: InputDecoration(labelText: 'Company'),
                onChanged: (value) {
                  setState(() {
                    company = value;
                  });
                },
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please input Company';
                  }
                  return null;
                },
              ),
              TextFormField(
                controller: _ageController,
                decoration: InputDecoration(labelText: 'Age'),
                onChanged: (value) {
                  setState(() {
                    age = value;
                  });
                },
                validator: (value) {
                  if (value == null ||
                      value.isEmpty ||
                      int.tryParse(value) == null) {
                    return 'Please nput Age';
                  }
                  return null;
                },
              ),
              ElevatedButton(
                  onPressed: () async {
                    if (_formKey.currentState!.validate()) {
                      if (userId.isEmpty) {
                        await addUser();
                      } else {
                        await updateUser();
                      }

                      Navigator.pop(context);
                    }
                  },
                  child: Text('Save'))
            ],
          )),
    );
  }
}
